import xbmcaddon
import xbmcgui
import xbmc
import os
import sys

addonHandle = xbmcaddon.Addon()
addonName = addonHandle.getAddonInfo('name')
addonIcon = addonHandle.getAddonInfo('icon')

def log(txt):
    #message = '%s: %s' % (addonName, str(txt).encode('ascii', 'ignore'))
    #xbmc.log(msg=message, level=xbmc.LOGINFO)
    pass

class VolumeController:
    controllerTypeList  =   ['hardware', 'software']
    controllerInputList =   ['I2S', 'SPDIF']
    controllerVolumeList=   [0, 100]
    controllerNumberList=   [0, 100]

    def __init__(self):
        #TODO: get from card capabilities (amixer -c 0 scontents)
        pass
    ########################
    # GENERIC
    ########################
    def getSettingValue(self, setting):
        if setting is 'controllerType':
            return addonHandle.getSettingString(setting)
        elif setting is 'controllerInput':
            return addonHandle.getSettingString(setting)
        elif setting is 'controllerVolume':
            return addonHandle.getSettingInt(setting)
        elif setting is 'controllerNumber':
            return addonHandle.getSettingInt(setting)

    def setSettingValue(self, setting, value):
        if setting is 'controllerType':
            settingList = self.controllerTypeList
            if value in settingList:
                log('current ' + setting + ' = ' + value)
                addonHandle.setSettingString(setting, value)
            else:
                log('setting ' + setting + ' value ' + value + ' is unavailable')
        elif setting is 'controllerInput':
            settingList = self.controllerInputList
            log('Available ' + setting + ' sources:')
            for input in settingList:
                log(' - ' + input)
            if value in settingList:
                log('current ' + setting + ' = ' + value)
                addonHandle.setSettingString(setting, value)
            else:
                log('setting ' + setting + ' value ' + value + ' is unavailable')
        elif setting is 'controllerVolume':
            settingList = self.controllerVolumeList
            if settingList[0] <= value <= settingList[1]:
                log('current ' + setting + ' = ' + str(value))
                addonHandle.setSettingInt(setting, value)
            else:
                log('setting ' + setting + ' value ' + str(value) + ' is unavailable')
        elif setting is 'controllerNumber':
            settingList = self.controllerNumberList
            if settingList[0] <= value <= settingList[1]:
                log('current ' + setting + ' = ' + str(value))
                addonHandle.setSettingInt(setting, value)
            else:
                log('setting ' + setting + ' value ' + str(value) + ' is unavailable')
        else:
            log('setting ' + setting + ' does not exist')

    ########################
    # VOLUME
    ########################
    def getVolume(self):
        return self.getSettingValue('controllerVolume')

    def setVolume(self, value):
        self.setSettingValue('controllerVolume', value)
        if self.getType() == 'hardware':
            cmd = "amixer -c " + str(self.getCardNumber()) + " sset 'Digital' " + str(self.getVolume())
            os.system(cmd)
            xbmcgui.Dialog().notification('Volume = ' + str(self.getVolume()) + '%', addonName, addonIcon, 1000)
        elif self.getType() == 'software':
            xbmc.executebuiltin('SetVolume(' + str(self.getVolume()) + ',showVolumeBar)')


    def setVolumeUp(self):
        self.setVolume(self.getVolume()+2)

    def setVolumeDown(self):
        self.setVolume(self.getVolume()-2)

    def setVolumeUpUp(self):
        self.setVolume(self.getVolume()+10)

    def setVolumeDownDown(self):
        self.setVolume(self.getVolume()-10)

    ########################
    # INPUT
    ########################
    def getInput(self):
        return self.getSettingValue('controllerInput')

    def setInput(self, value):
        self.setSettingValue('controllerInput', value)
        cmd = "amixer -c " + str(self.getCardNumber()) + " sset 'I2S/SPDIF Select' " + self.getInput()
        os.system(cmd)
        xbmcgui.Dialog().notification('Input = ' + self.getInput(), addonName, addonIcon, 1000)

    def switchInput(self):
        self.setInput(self.controllerInputList[(self.controllerInputList.index(self.getInput())+1)%len(self.controllerInputList)])

    ########################
    # CARD NUMBER
    ########################
    def getCardNumber(self):
        return self.getSettingValue('controllerNumber')

    ########################
    # TYPE
    ########################
    def getType(self):
        return self.getSettingValue('controllerType')

    def setType(self, value):
        self.setSettingValue('controllerType', value)
        xbmcgui.Dialog().notification('Type = ' + self.getType(), addonName, addonIcon, 1000)

    def switchType(self):
        self.setType(self.controllerTypeList[(self.controllerTypeList.index(self.getType())+1)%len(self.controllerTypeList)])

if __name__ == "__main__":
    # Create VolumeController instance:
    controller = VolumeController()
    # Check arguments:
    if len(sys.argv) < 2:
        log('No input argument... leaving')
        sys.exit(0)
    addonArg = sys.argv[1]
    log(str(addonArg))
    # case <f1>RunAddon(script.module.i-sabre,volumeUp)</f1>
    if addonArg == 'volumeUp':
        controller.setVolumeUp()
    # case <f2>RunAddon(script.module.i-sabre,volumeDown)</f2>
    elif addonArg == 'volumeDown':
        controller.setVolumeDown()
    # case <f1>RunAddon(script.module.i-sabre,volumeUp)</f1>
    elif addonArg == 'volumeUpUp':
        controller.setVolumeUpUp()
    # case <f2>RunAddon(script.module.i-sabre,volumeDown)</f2>
    elif addonArg == 'volumeDownDown':
        controller.setVolumeDownDown()
    # case from service.osmc.btplayer
    elif addonArg.find('volume=') > -1:
        value = addonArg.replace('volume=', '')
        if value.isdigit():
            controller.setVolume(int(value))
        else:
            log('Bad volume input argument')
    # case <f13>RunAddon(script.module.i-sabre,switchInput)</f3>
    elif addonArg == 'switchInput':
        controller.switchInput()
