# kodi_dac-i-sabre-es90XX

Plugin for Audiophonics DAC i-sabre ES9038q2m

## Kodi Documentation (plugin dev)

* [tutorial](https://kodi.wiki/view/Audio-video_add-on_tutorial#Working_with_user_settings)

* [plugin development](https://kodi.wiki/view/Add-on_development)

* [Kodi development](https://codedocs.xyz/xbmc/xbmc/index.html)

## To do

# keymap

* Disable eventlircd service at startup:
~~~~
sudo systemctl stop eventlircd
sudo systemctl disable eventlircd
~~~~
* Remove old `keymap editor` files if exist:
~~~~
rm ~/.kodi/userdata/keymaps/gen.xml
~~~~
* Add OSMC remote keymap file:
~~~~
cp ~/.kodi/addons/script.module.i-sabre/resources/keyboard.xml ~/.kodi/userdata/keymaps
~~~~
